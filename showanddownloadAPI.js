var express = require('express');
const axios = require('axios');
var app = express();
const json2csv = require('json2csv').parse;

//ShowJson API
app.get('/showJson', function (req, res) {
	showJson().then((value) => {res.header("Content-Type",'application/json');
	res.send(JSON.stringify(value,undefined,4));
})
})

// DownloadCSV API
app.get('/downloadCSV', function (req, res) {
 showJson().then((value) => { 
	const csv = json2csv(value);
	res.header('Content-Type', 'text/csv');
	res.attachment('UserPosts.csv');
	return res.send(csv);
}) 
})

//Asynchronous function call to get posts and users json and merge to output json
async function showJson() {
	//Get the posts json
    let posts = await axios.get('https://jsonplaceholder.typicode.com/posts');
    let updatedResult = posts.data.map(async post => {
		//Get the Users json for each userID
        let users = await axios.get('https://jsonplaceholder.typicode.com/users' + '?id='.concat(post.userId.toString())); 
		//Remove the userId attribute
		delete post['userId'];
		//Set the username and email for all posts
		post.username = users.data[0].username;
		post.email = users.data[0].email;
        return post; 
    });
//Return the final json once all promises are resolved
    let showjson = await Promise.all(updatedResult)
        .then(finalResult => {return finalResult;})
		.catch(error => console.log(`Error in promises ${error}`))
	return showjson;	
}

// START THE SERVER AND LISTEN ON PORT 8081
// ===================
var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port  
   console.log("Example app listening at http://%s:%s", host, port)
})