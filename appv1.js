var express = require('express');
const axios = require('axios');
var app = express();
axios.defaults.timeout = 60000;


app.get('/v1/showJson', function (req, res) {
	//res.end(JSON.stringify({"foo": "bar"}));
	var samplejson;
	axios.get('https://jsonplaceholder.typicode.com/posts')
    .then(response => {
        console.log(response.data.status);
        console.log(response.data);
		samplejson= response.data;
        res.send(JSON.stringify(response.data));
    })
    .catch(error => {
        console.log(error);
    });
  //res.json(samplejson);

})

app.get('/v2/showJson', function (req, res) {
	//res.end(JSON.stringify({"foo": "bar"}));
	var samplejson;
	axios.get('https://jsonplaceholder.typicode.com/posts')
    .then(response => {
        console.log(response.data.status);
        console.log(response.data);
		samplejson= response.data;
        res.send(JSON.stringify(response.data));
    })
    .catch(error => {
        console.log(error);
    });
  //res.json(samplejson);

})


app.get('/v3/showJson', function (req, res) {

getPeople().then((value) => {console.log(value);res.send(value);})
})



app.get('/v4/showJson', function (req, res) {
async function getPeople() {
    let posts = await axios.get('https://jsonplaceholder.typicode.com/posts');
    // firstResult = [{name: "John", id: 1}, {name: "Anna", id: 2}]
console.log(posts);
    let updatedResult = posts.data.map(async item => {
        let users = await axios.get('https://jsonplaceholder.typicode.com/users' + '?id='.concat(item.userId.toString())); // or however your endpoint is designed 
        // people = {name: "John", id: 1, gender: male}
        //item.gender = people.gender;
		console.log('users: ',users.data);
		console.log('Before: ',item);
		item.username = users.data[0].username;
		console.log('After :',item);
        return item; 
    });
    // updatedResult = undefined

  let json=  await Promise.all(updatedResult)
        .then(finalResult => {return finalResult;console.log("Final json: ",finalResult)})
		.catch(error => console.log(`Error in promises ${error}`))
    //  [{name: "John", id: 1, gender: male}, {name: "Anna", id: 2, gender: female}]
	
	res.send(json);
}

})

async function getPeople() {
    let posts = await axios.get('https://jsonplaceholder.typicode.com/posts');
    // firstResult = [{name: "John", id: 1}, {name: "Anna", id: 2}]
console.log(posts);
    let updatedResult = posts.data.map(async item => {
        let users = await axios.get('https://jsonplaceholder.typicode.com/users' + '?id='.concat(item.userId.toString())); // or however your endpoint is designed 
        // people = {name: "John", id: 1, gender: male}
        //item.gender = people.gender;
		console.log('users: ',users.data);
		console.log('Before: ',item);
		item.username = users.data[0].username;
		console.log('After :',item);
        return item; 
    });
    // updatedResult = undefined

    let showjson = await Promise.all(updatedResult)
        .then(finalResult => {console.log("Final json: ",finalResult);return finalResult;})
		.catch(error => console.log(`Error in promises ${error}`))
    //  [{name: "John", id: 1, gender: male}, {name: "Anna", id: 2, gender: female}]
	console.log(showjson);
	return showjson;
	console.log('*******');
	
}
var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})