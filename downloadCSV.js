var express = require('express');
const axios = require('axios');
var app = express();
const json2csv = require('json2csv').parse;
//import { Parser } from 'json2csv';
// Set config defaults when creating the instance
/*const instance = axios.create({
  baseURL: 'http://localhost:8081'
});*/
axios.defaults.baseURL = 'http://localhost:8081';

var someData = [
    {
        "Country": "Nigeria",
        "Population": "200m",
        "Continent": "Africa",
        "Official Language(s)": "English"
    },
    {
        "Country": "India",
        "Population": "1b",
        "Continent": "Asia",
        "Official Language(s)": "Hindi, English"
    },
    {
        "Country": "United States of America",
        "Population": "328m",
        "Continent": "North America",
        "Official Language": "English"
    },
    {
        "Country": "United Kingdom",
        "Population": "66m",
        "Continent": "Europe",
        "Official Language": "English"
    },
    {
        "Country": "Brazil",
        "Population": "209m",
        "Continent": "South America",
        "Official Language": "Portugese"
    }
]
app.get('/downloadCSV', function (req, res) {
 var fs = require('fs');
var stringify = require('csv-stringify');
    
stringify(someData, {
    header: true
}, function (err, output) {
	console.log(output);
    fs.writeFile("somedata.csv", output, (err) => { 
  if (err) 
    console.log(err); 
  else { 
    console.log("File written successfully\n"); 
  } 
}); 
})
 res.download('somedata.csv', function(error){ 
        console.log("Error : ", error) 
    }); 
})

app.get('/v1/downloadCSV', function (req, res) {
  json2csv(someData, function(err, csv) {
    res.setHeader('Content-disposition', 'attachment; filename=data.csv');
    res.set('Content-Type', 'text/csv');
    res.status(200).send(csv);
  });
})

app.get('/v4/downloadCSV', function (req, res) {
  //const json2csv = new Parser();
  const csv = json2csv(someData);
  res.header('Content-Type', 'text/csv');
  res.attachment('testjson.csv');
  return res.send(csv);

})

app.get('/v2/downloadCSV', function (req, res) {
  axios
        .get('/showJson', {
            headers: this.headers,
            responseType: 'blob', // had to add this one here
        })
        .then(response => {
           const content = response.headers['content-type'];
           console.log(response.data);
        })
        .catch(error => console.log(error));
})
var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})