var express = require('express');
const axios = require('axios');
var app = express();

app.get('/showJson', function (req, res) {
showJson().then((value) => {res.header("Content-Type",'application/json');res.send(JSON.stringify(value,undefined,4));
})
})


async function showJson() {
    let posts = await axios.get('https://jsonplaceholder.typicode.com/posts');
    let updatedResult = posts.data.map(async item => {
        let users = await axios.get('https://jsonplaceholder.typicode.com/users' + '?id='.concat(item.userId.toString())); 
		delete item['userId'];
		item.username = users.data[0].username;
		item.email = users.data[0].email;
        return item; 
    });

    let showjson = await Promise.all(updatedResult)
        .then(finalResult => {return finalResult;})
		.catch(error => console.log(`Error in promises ${error}`))
	return showjson;	
}
var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})